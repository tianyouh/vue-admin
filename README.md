# To start

This is a project template for [vue-cli](https://github.com/vuejs/vue-cli)

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8081
npm run dev

# build for production with minification
npm run build

```
* * *
# 自定义色彩主题

## 安装工具

首先安装「主题生成工具」，可以全局安装或者安装在当前项目下，推荐安装在项目里，方便别人 clone 项目时能直接安装依赖并启动，这里以全局安装做演示。

`npm i element-theme -g`
安装默认主题，可以从 npm 安装或者从 GitHub 拉取最新代码。

#### 从 npm
`npm i element-theme-default -D`

#### 从 GitHub
`npm i https://github.com/ElementUI/theme-default -D`

## 初始化变量文件

主题生成工具安装成功后，如果全局安装可以在命令行里通过 et 调用工具，如果安装在当前目录下，需要通过 node_modules/.bin/et 访问到命令。执行 -i 初始化变量文件。默认输出到 element-variables.css，当然你可以传参数指定文件输出目录。

et -i [可以自定义变量文件]

> ✔ Generator variables file

如果使用默认配置，执行后当前目录会有一个 element-variables.css 文件。内部包含了主题所用到的所有变量，它们使用 CSS4 的风格定义.

## 修改变量

找到 element-variables.css 文件，例如修改主题色为红色。

`--color-primary: red;`

## 编译主题

保存文件后，到命令行里执行 et 编译主题，如果你想启用 watch 模式，实时编译主题，增加 -w 参数；如果你在初始化时指定了自定义变量文件，则需要增加 -c 参数，并带上你的变量文件名

`et`

> ✔ build theme font  
> ✔ build element theme

## 引入自定义主题

默认情况下编译的主题目录是放在 ./theme 下，你可以通过 -o 参数指定打包目录。像引入默认主题一样，在代码里直接引用 theme/index.css 文件即可。

```
import '../theme/index.css'  
import ElementUI from 'element-ui'//无需再导入原有主题
import Vue from 'vue'
Vue.use(ElementUI)
````
