import Vue from "vue"
import VueRouter from "vue-router"
import store from "./vuex"

import Login from "./views/Login.vue"

import NotFound from "./views/404.vue"

import Home from "./views/Home.vue"

import User from "./views/systemgl/User.vue"
import Bmzd from "./views/systemgl/Bmzd.vue"
import Role from "./views/systemgl/Role.vue"
import Qxgl from "./views/systemgl/Qxgl.vue"
import Param from "./views/systemgl/Param.vue"
import Test from "./views/systemgl/Test.vue"

Vue.use(VueRouter);

const routes = [
    {
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    },
    {
        path: "/404",
        component: NotFound,
        name: "",
        hidden: true
    },
    {
        path: '/',
        meta: {
            requireAuth: true
        },
        component: Home,
        name: '系统管理',
        iconCls: 'fa fa-gear',
        children: [
            {path: '/User', component: User, name: '用户管理'},
            {path: '/Bmzd', component: Bmzd, name: '部门管理'},
            {path: '/Role', component: Role, name: '角色管理'},
            {path: '/Param', component: Param, name: '参数管理'},
            {path: '/Qxgl', component: Qxgl, name: '权限管理'},
            {path: '/Test', component: Test, name: '测试页面',hidden:true},
        ]
    },
    {
        path: '*',
        hidden: true,
        redirect: {path: '/404'}
    }
];



// 页面刷新时重新赋值token
if (window.localStorage.getItem("user")) {
    let user = window.localStorage.getItem("user");
        user = JSON.parse(user);
        store.dispatch("addUser", user)
}
if (window.localStorage.getItem("isSave")) {
    let isSave = window.localStorage.getItem("isSave")
        isSave = JSON.parse(isSave)
        store.dispatch("save", isSave)
        if (isSave) {
            if (window.localStorage.getItem("account")) {
                let account = window.localStorage.getItem("account")
                account = JSON.parse(account)
                store.dispatch("saveAcc", account)
            }
        }
}

const router = new VueRouter({routes});

router.beforeEach((to, from, next) => {
    if (to.matched.some(r => r.meta.requireAuth)) { // 判断该路由是否需要登录权限
        if (store.getters.token) { // 通过vuex state获取当前的token是否存在
            next();
        } else {
            next({
                path: "/login",
                query: {redirect: to.fullPath} // 将跳转的路由path作为参数，登录成功后跳转到该路由
            })
        }
    } else {
        next();
    }
});

export default router;