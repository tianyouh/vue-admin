import axios from './http';

let prefix = "/api/";
// 登录
export const requestLogin = params => {
    return axios.post(`${prefix}login`, params).
    then(res => res.data);
};

// 获取用户管理列表
export const getUserList = params => {
    return axios.get(`${prefix}user/index`, {params: params});
};
// 删除用户
export const deleteUser = params => {
    return axios.delete(`${prefix}user/delete`, { params: params});
};
// 用户列表批量删除
export const deleteUserList = params => {
    return axios.delete(`${prefix}user/deleteAll`, { params: params});
};
// 用户列表编辑
export const editUser = params => {
    return axios.put(`${prefix}user/update`, params);
};
// 用户列表新增用户
export const addUser = params => {
    return axios.post(`${prefix}user/save`, params);
};
//用户密码修改
export const updatePassword = params => {
    return axios.post(`${prefix}user/changepwd`, params)
};
// 获取参数管理列表
export const getParamList = params => {
    return axios.get(`${prefix}lparameter/index`, {params: params});
};
// 新增参数数据
export const addParam = params => {
    return axios.post(`${prefix}lparameter/save`, params);
};
// 编辑参数数据
export const editParam = params => {
    return axios.put(`${prefix}lparameter/update`, params);
};
// 删除参数数据
export const deleteParam = params => {
    return axios.delete(`${prefix}lparameter/delete`, {params: params});
};

// 删除所有参数数据
export const deleteParamAll = params => {
    return axios.delete(`${prefix}lparameter/deleteAll`, {params: params});
};

// 获取直播信息列表
export const getLliveList = params => {
    return axios.get(`${prefix}llive/online`, {params: params});
};


//删除直播信息
export const deleteLlive = params => {
    return axios.delete(`${prefix}llive/delete`, {params: params});
};

//删除全部直播信息
export const deleteLliveALl = params => {
    return axios.delete(`${prefix}llive/deleteAll`, {params: params});
};

//是否启用直播
export  const setKszb = params => {
    return axios.post(`${prefix}llive/liveOpen`,params);
};

//获取部门树列表
export const getBmzdTree = params => {
    axios.get(`${prefix}lbmzd/tree`, {params: params}).then((response) => {
        return Promise.resolve(response);
    },(error) => {
        return Promise.reject(error);
    });
    return axios.get(`${prefix}lbmzd/tree`, {params: params});
};

//新建部门树节点
export  const addBmzdTree= params => {
    return axios.post(`${prefix}lbmzd/save`, params)
};

//编辑部门树节点
export  const editBmzdTree= params => {
    return axios.put(`${prefix}lbmzd/update`, params)
};

//获取部门详情
export const getBmzd = params => {
    return axios.get(`${prefix}lbmzd/show`, {params:params});
};

//删除树节点
export const remBmzdTree = params => {
    return axios.delete(`${prefix}lbmzd/delete`, {params: params})
};

//获取角色用户表
export const  getRoleList = params => {
    return axios.get(`${prefix}role/index`, {params: params});
};

//更新角色权限
export const  savePermission = params => {
    return axios.post(`${prefix}role/savePermission`, params);
};


// 角色列表编辑
export const editRole = params => {
    return axios.put(`${prefix}role/update`, params);
};

// 角色列表新增
export const addRole = params => {
    return axios.post(`${prefix}role/save`, params);
};

//角色列表删除
export const deleteRole = params => {
    return axios.delete(`${prefix}role/delete`, { params: params});
};

// 角色批量删除
export const deleteRoleList = params => {
    return axios.delete(`${prefix}role/deleteAll`, { params: params});
};

//获取语音列表
export const  getLdiffusionList = params => {
    return axios.get(`${prefix}ldiffusion/index`, {params: params});
};

//载入播放语音
export  const  getYuYin = url => {
    return axios.get(url,{responseType: "blob"});
};

//根据权限获取厨师统计列表
export const getCstjTree = params => {
    return axios.get(`${prefix}lbmzd/userTree`, {params: params});
};

//获取厨师信息
export  const getCsxxList = params => {
    return axios.get(`${prefix}lkitchener/index`, {params: params});
};

//增加厨师信息
export const addCsxx = params => {
    return axios.post(`${prefix}lkitchener/save`, params);
};

//编辑厨师信息
export const editCsxx = params => {
    return axios.put(`${prefix}lkitchener/update`, params);
};

//删除厨师信息
export const removeCsxxList = params => {
    return axios.delete(`${prefix}lkitchener/delete`, {params :params});
};

//批量删除厨师信息
export  const removeCxssAll = params => {
    return axios.delete(`${prefix}lkitchener/deleteAll`, {params : params});
};

//统计厨师信息
export  const kstaticByProvince = params => {
    return axios.get(`${prefix}lkitchener/staticByProvince`, {params: params});
};

//统计市数据信息
export  const kstaticByCity = params => {
    return axios.get(`${prefix}lkitchener/staticByCity`, {params: params});
};

//统计省数据信息
export  const staticByProvince = params => {
    return axios.get(`${prefix}llive/staticByProvince`, {params: params});
};

//统计市数据信息
export  const staticByCity = params => {
    return axios.get(`${prefix}llive/staticByCity`, {params: params});
};

// 获取直播信息列表
export const getLliveHostory = params => {
    return axios.get(`${prefix}llive/history`, {params: params});
};

//统计直播历史图片
export  const getLliveImages = params => {
    return axios.get(`${prefix}rtmp/images`, {params: params});
};

//权限接口
export  const getController = params => {
    return axios.get(`${prefix}controller/index`, {params: params});
};
// 编辑权限
export const editController = params => {
    return axios.put(`${prefix}controller/update`, params);
}
// 编辑权限
export const editAction = params => {
    return axios.put(`${prefix}controller/actionupdate`, params);
}

//删除权限
export  const removeController = params => {
    return axios.delete(`${prefix}controller/delete`, {params: params});
};

//多个删除权限
export  const removeControllerAll = params => {
    return axios.delete(`${prefix}controller/deleteAll`, {params: params});
};

// 导出
export const getLkitchenerExcel = params => {
    return axios.get(`${prefix}lkitchener/exportExcel`, {params: params});
}
export const getUserExcel = params => {
    return axios.get(`${prefix}user/exportExcel`, {params: params});
}
