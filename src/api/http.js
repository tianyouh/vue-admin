import axios from 'axios';
import store from "../vuex";
import router from "../router";

// axios配置
axios.defaults.timeout = 10000;
// axios.defaults.baseURL = "http://live.leshi360.com:8080"
axios.defaults.baseURL = "http://192.168.1.207:8081";
// axios.defaults.baseURL = "";

// http request 拦截器
axios.interceptors.request.use(
    config => {
        if(DEV)console.log("发起请求");
        if(DEV)console.log(config.data);
        if (store.getters.token) {
            config.headers['x-auth-token'] = store.getters.token;
        }
        return config
    },
    error => {
        return Promise.reject(err);
    }
);
// http response 拦截器
axios.interceptors.response.use(
    response => {
        if(DEV)console.log("响应成功");
        return response;
    },
    error => {
        if(DEV)console.log("响应失败");
        if (error.response) {
            switch(error.response.status){
                case 401://未经授权
                    store.dispatch("removeUser")
                    router.replace({
                        path: "login",
                        query: {redirect: router.currentRoute.fullPath}
                    });
                    break;
                case 403:
                    return Promise.reject(`无权限操作${error.response.status}`);
                case 404:
                    return Promise.reject(`请求资源不存在${error.response.status}`);
                case 500:
                    console.log(error.response);
                    return Promise.reject(`服务器遇到错误，无法完成请求。${error.response.status}`);
                default:
                    if(DEV)console.log(error);
                    return Promise.reject(`未知错误,请联系客服${error.response.status}`);
            }
        }else {
            if(DEV)console.log(error);
            return Promise.reject(error);
        }
    }
);

export default axios;