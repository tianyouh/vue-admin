import * as types from "./types"
import getters from './getters'

const state = {
    user: {},
    name: "农村聚餐管理系统",
    isSave: false,
    account:{}
}
const mutations = {
    [types.ADDUSERNAME]: (state, data) => {
        localStorage.user = JSON.stringify(data);
        state.user = data;
    },
    [types.REMUSERNAME]: (state) => {
        localStorage.removeItem("user");
        state.user = {};
    },
    [types.SAVE]: (state, data) => {
        localStorage.isSave = JSON.stringify(data);
        state.isSave = data;
    },
    [types.SAVEACCOUNT]: (state, data) => {
        localStorage.account = JSON.stringify(data);
        state.account = data;
    },
    [types.REMOVACCOUNT]: (state) => {
        localStorage.removeItem("account");
        state.account = {};
    }
}

export default {
    state,
    mutations,
    getters
}