import * as types from './types'

export default {
    addUser: ({commit}, data) => {
      commit(types.ADDUSERNAME, data)
    },
    removeUser: ({commit}) => {
      commit(types.REMUSERNAME)
    },
    save: ({commit}, data) => {
        commit(types.SAVE, data)
    },
    saveAcc: ({ commit , state}, data) => {
        if (state.mutations.isSave) {
          commit(types.SAVEACCOUNT, data)
        }
    },
    removeAcc: ({ commit }) => {
        commit(types.REMOVACCOUNT)
    }
    
}