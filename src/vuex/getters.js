export default {
    name: (state) => {
        return state.name
    },
    account: (state) => {
        return state.account
    },
    token: (state) => {
      return state.user.access_token
    },
    saveChecked: (state) => {
        return state.isSave
    }
}