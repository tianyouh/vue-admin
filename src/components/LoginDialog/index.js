import LoginComponent from "./LoginDialog.vue"

LoginComponent.install = function (Vue) {
    Vue.component(LoginComponent.name, LoginComponent)
}

export default LoginComponent;