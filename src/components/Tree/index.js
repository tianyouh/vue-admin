let ElTreeGrid = require('element-tree-grid');//树形表

ElTreeGrid.install = function (Vue) {
  Vue.component(ElTreeGrid.name, ElTreeGrid)
}
export default ElTreeGrid