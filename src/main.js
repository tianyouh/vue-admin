require('./bootstrap')

import Vue from 'vue'
import App from './App.vue'
import '../theme/index.css'
import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-default/index.css'

import store from './vuex'

import router from './router'

import 'font-awesome/css/font-awesome.min.css'

import ElTreeGrid from './components/Tree'

import VueImg from "v-img"

import hLogin from './components/LoginDialog';

Vue.use(ElTreeGrid);

Vue.use(hLogin);

Vue.use(VueImg)
Vue.use(ElementUI);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');

